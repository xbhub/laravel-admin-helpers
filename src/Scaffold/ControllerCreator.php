<?php

namespace Xbhub\Admin\Helpers\Scaffold;

class ControllerCreator
{
    /**
     * Controller full name.
     *
     * @var string
     */
    protected $name;

    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    protected $DummyGridField = '';

    protected $DummyShowField = '';

    protected $DummyFormField = '';
    protected $module;

    /**
     * ControllerCreator constructor.
     *
     * @param string $name
     * @param null $files
     */
    public function __construct($module, $name, $files = null)
    {
        $this->name = $name;
        $this->module = $module;

        $this->files = $files ?: app('files');
    }

    /**
     * Create a controller.
     *
     * @param string $model
     *
     * @return string
     * @throws \Exception
     *
     */
    public function create($model, $fields)
    {
        $path = $this->getpath($this->name);

        if ($this->files->exists($path)) {
            throw new \Exception("Controller [$this->name] already exists!");
        }

        $this->generateGridField($fields);

        $this->generateShowField($fields);

        $this->generateFormField($fields);

        $stub = $this->files->get($this->getStub());

        $this->files->put($path, $this->replace($stub, $this->name, $model));

        return $path;
    }

    /**
     * @param string $stub
     * @param string $name
     * @param string $model
     *
     * @return string
     */
    protected function replace($stub, $name, $model)
    {
        $stub = $this->replaceClass($stub, $name);

        return str_replace(
            ['DummyModelNamespace', 'DummyModel', 'DummyGridField', 'DummyShowField', 'DummyFormField'],
            [$this->getModelNamespcae($model), class_basename($model), $this->DummyGridField, $this->DummyShowField, $this->DummyFormField],
            $stub
        );
    }

    protected function getModelNamespcae($model)
    {
        return $this->getNamespace($model) . '\\' . $this->getClassName($model);
    }

    protected function getClassName($name)
    {
        $segments = explode('\\', $name);
        return $segments[count($segments) - 1];
    }

    /**
     * Get controller namespace from giving name.
     *
     * @param string $name
     *
     * @return string
     */
    protected function getNamespace($name)
    {
        $moduleNamespace = app('config')->get('modules.namespace');
        $namespace = trim(implode('\\', array_slice(explode('\\', $name), 0, -1)), '\\');
        return $this->module ? $moduleNamespace . '\\' . $this->module . '\\' . $namespace : $namespace;
    }

    /**
     * Replace the class name for the given stub.
     *
     * @param string $stub
     * @param string $name
     *
     * @return string
     */
    protected function replaceClass($stub, $name)
    {
        return str_replace(['DummyClass', 'DummyNamespace'], [$this->getClassName($name), $this->getNamespace($name)], $stub);
    }

    /**
     * Get file path from giving controller name.
     *
     * @param $name
     *
     * @return string
     */
    public function getPath($name, $onlyName = false)
    {
        $segments = explode('\\', $name);

        if ($onlyName) {
            array_shift($segments);
        }

        if ($this->module) {
            $r = module_path($this->module, implode('/', $segments)) . '.php';
        } else {
            $r = app_path(implode('/', $segments)) . '.php';
        }
        return $r;
    }

    /**
     * Get stub file path.
     *
     * @return string
     */
    public function getStub()
    {
        return __DIR__ . '/stubs/controller.stub';
    }

    public function generateFormField($fields = [])
    {
        $fields = array_filter($fields, function ($field) {
            return isset($field['name']) && !empty($field['name']);
        });

        if (empty($fields)) {
            throw new \Exception('Table fields can\'t be empty');
        }

        foreach ($fields as $field) {
            $rows[] = "\$form->text('{$field['name']}', '{$field['name']}');\n";
        }

        $this->DummyFormField = trim(implode(str_repeat(' ', 8), $rows), "\n");

        return $this;
    }

    public function generateShowField($fields = [])
    {
        $fields = array_filter($fields, function ($field) {
            return isset($field['name']) && !empty($field['name']);
        });

        if (empty($fields)) {
            throw new \Exception('Table fields can\'t be empty');
        }
        foreach ($fields as $field) {
            $rows[] = "\$show->{$field['name']}('{$field['name']}');\n";
        }

        $this->DummyShowField = trim(implode(str_repeat(' ', 8), $rows), "\n");

        return $this;
    }

    public function generateGridField($fields = [])
    {
        $fields = array_filter($fields, function ($field) {
            return isset($field['name']) && !empty($field['name']);
        });

        if (empty($fields)) {
            throw new \Exception('Table fields can\'t be empty');
        }
        foreach ($fields as $field) {
            $rows[] = "\$grid->{$field['name']}('{$field['name']}');\n";
        }

        $this->DummyGridField = trim(implode(str_repeat(' ', 8), $rows), "\n");

        return $this;
    }
}
